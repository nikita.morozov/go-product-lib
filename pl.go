package product_lib

import (
	"gitlab.com/nikita.morozov/go-product-lib/repositories"
	"gorm.io/gorm"
)

func Init[Product any, Stage any](db *gorm.DB, limit int, model Product, stageModel Stage) (repositories.ProductRepoRepo[Product], repositories.StageRepoRepo[Stage], error) {
	err := repositories.NewGormMigration(db, model, stageModel).Migrate()
	if err != nil {
		return nil, nil, err
	}

	productRepo := repositories.NewProductRepository[Product](db, limit)
	stageRepo := repositories.NewStageRepository[Stage](db, limit)

	return productRepo, stageRepo, err
}
