module gitlab.com/nikita.morozov/go-product-lib

go 1.18

require (
	gitlab.com/nikita.morozov/ms-shared v1.9.8
	gorm.io/gorm v1.23.8
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
)
