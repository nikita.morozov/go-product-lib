package models

import (
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type BaseProduct[Stage any] struct {
	sharedModels.BaseUInt64Model
	Stages []Stage `json:"stages"`
}
