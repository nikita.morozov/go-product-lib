package dto

type ProductOptions struct {
	ProductPreloading *[]string
	JoinProduct       *[]string
}
