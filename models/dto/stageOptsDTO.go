package dto

type StageOptions struct {
	PreloadingStage *[]string
	JoinStage       *[]string
}
