package models

import sharedModels "gitlab.com/nikita.morozov/ms-shared/models"

type BaseStage struct {
	sharedModels.BaseUInt64Model
	ProductId *uint64 `json:"productId" gorm:"not null;"`
}
