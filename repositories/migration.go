package repositories

import (
	"gorm.io/gorm"
)

type Migration interface {
	Migrate() error
}

type gormMigration struct {
	DB         *gorm.DB
	model      any
	stageModel any
}

func (g gormMigration) Migrate() error {
	var err error
	err = g.DB.Migrator().AutoMigrate(g.model)
	if err != nil {
		return err
	}
	err = g.DB.Migrator().AutoMigrate(g.stageModel)
	return err
}

func NewGormMigration[Product any, Stage any](DB *gorm.DB, model Product, stageModel Stage) Migration {
	return &gormMigration{
		DB:         DB,
		model:      model,
		stageModel: stageModel,
	}
}
