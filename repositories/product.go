package repositories

import (
	"gitlab.com/nikita.morozov/go-product-lib/models/dto"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type ProductRepoRepo[Product any] interface {
	Create(item *Product) error
	Update(item *Product) error
	List(opts *sharedModels.ListOptions, optsReq *dto.ProductOptions) (*[]Product, error)
	Count() (int64, error)
	Delete(id uint64) error
	GetById(id uint64) (*Product, error)
}

type gormProductRepository[Product any] struct {
	DB    *gorm.DB
	limit int
}

func (r *gormProductRepository[Product]) Create(item *Product) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormProductRepository[Product]) Update(item *Product) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormProductRepository[Product]) List(opts *sharedModels.ListOptions, optsReq *dto.ProductOptions) (*[]Product, error) {
	var items []Product

	result := r.DB.Preload("Stages")

	if optsReq != nil {
		if optsReq.ProductPreloading != nil {
			for _, item := range *optsReq.ProductPreloading {
				result = result.Preload(item)
			}
		}

		if optsReq.JoinProduct != nil {
			for _, item := range *optsReq.JoinProduct {
				result = result.Joins(item)
			}
		}
	}

	if opts != nil {
		result = result.Limit(opts.GetLimit(r.limit))
		result = result.Offset(opts.Offset)
		result = result.Order(opts.GetOrder())
	}

	result = result.Find(&items)
	return &items, result.Error
}

func (r *gormProductRepository[Product]) Count() (int64, error) {
	var count int64
	var item Product
	result := r.DB.Model(&item).Count(&count)
	return count, result.Error
}

func (r *gormProductRepository[Product]) Delete(id uint64) error {
	var item Product
	result := r.DB.Model(&item).Delete(&id)
	return result.Error
}

func (r *gormProductRepository[Product]) GetById(id uint64) (*Product, error) {
	var item Product
	result := r.DB.Preload("Stages")

	//for _, val := range r.preloading {
	//	result = result.Preload(val)
	//}

	result = result.Find(&item, id)
	return &item, result.Error
}

func NewProductRepository[Product any](DB *gorm.DB, limit int) ProductRepoRepo[Product] {
	return &gormProductRepository[Product]{
		DB:    DB,
		limit: limit,
	}
}
