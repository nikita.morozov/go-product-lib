package repositories

import (
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type StageRepoRepo[Stage any] interface {
	Create(item *Stage) error
	Update(item *Stage) error
	List(opts *sharedModels.ListOptions) (*[]Stage, error)
	Count() (int64, error)
	Delete(id uint64) error
	GetById(id uint64) (*Stage, error)
}

type gormStageRepository[Stage any] struct {
	DB    *gorm.DB
	limit int
}

func (r *gormStageRepository[Stage]) Create(item *Stage) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormStageRepository[Stage]) Update(item *Stage) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormStageRepository[Stage]) List(opts *sharedModels.ListOptions) (*[]Stage, error) {
	var items []Stage

	result := r.DB

	//for _, item := range r.preloading {
	//	result = result.Preload(item)
	//}

	if opts != nil {
		result = result.Limit(opts.GetLimit(r.limit))
		result = result.Offset(opts.Offset)
		result = result.Order(opts.GetOrder())
	}

	result = result.Find(&items)
	return &items, result.Error
}

func (r *gormStageRepository[Stage]) Count() (int64, error) {
	var count int64
	var item Stage
	result := r.DB.Model(&item).Count(&count)
	return count, result.Error
}

func (r *gormStageRepository[Stage]) Delete(id uint64) error {
	var item Stage
	result := r.DB.Model(&item).Delete(&id)
	return result.Error
}

func (r *gormStageRepository[Stage]) GetById(id uint64) (*Stage, error) {
	var item Stage
	result := r.DB

	//for _, val := range r.preloading {
	//	result = result.Preload(val)
	//}

	result = result.Find(&item, id)
	return &item, result.Error
}

func NewStageRepository[Stage any](DB *gorm.DB, limit int) StageRepoRepo[Stage] {
	return &gormStageRepository[Stage]{
		DB:    DB,
		limit: limit,
	}
}
